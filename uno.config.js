import { defineConfig, presetUno, presetAttributify, transformerDirectives } from 'unocss'

export default defineConfig({
    presets: [
        presetAttributify({ /* preset options */ }),
        presetUno() // 添加 UnoCSS 的默认样式预设
    ],
    transformers: [
        transformerDirectives()
    ],
    rules: [
        // 自定义宽度
        [/^sd-w-(\d+)$/, ([, d]) => ({ width: `${d / 4}rem` })],
        // 自定义高度
        [/^sd-h-(\d+)$/, ([, d]) => ({ height: `${d / 4}rem` })],
        // 自定义行高
        [/^sd-l-h-(\d+)$/, ([, d]) => ({ lineHeight: `${d / 4}rem` })],
        // 自定义背景色
        [/^sd-bg-(.+)$/, ([, d]) => ({ background: `#${d}` })],
        // 自定义字体颜色
        [/^sd-c-(.+)$/, ([, d]) => ({ color: `#${d}` })],
        // 内置颜色
        [/^sd-(.+)$/, ([, d]) => ({ color: `var(--el-color-${d})`})],
        // 自定义flex-{d}
        [/^sd-flex-(\d+)$/, ([, d]) => ({ flex: `${d}` })],
        // 自定义border
        [/^(?:border|b)-(\d+)-(?:style-)?(.+)$/, ([, d, s]) => ({ border: `${d}px ${s}` })],
        // 自定义border-color
        [/^(?:border|b)-(\d+)-(?:style-)?(.+)-(.+)$/, ([, d, s, c]) => ({ border: `${d}px ${s} #${c}` })],
    ],
    shortcuts: [{
        // 横向居中布局
        'flex-center': 'flex items-center justify-center',
        // 右对齐布局
        'flex-end': 'flex items-center justify-end',
        // 垂直居中布局
        'flex-col-center': 'flex flex-col items-center',
    }]
})