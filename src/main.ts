import { createApp } from 'vue'
import App from './App.vue'
import router from "@/router"
import store from "@/store"
import apis from '@/apis'

import "@/styles/index.scss";
import "virtual:uno.css";
import 'nprogress/nprogress.css'


import 'element-plus/dist/index.css'


import components from '@/components/index.ts'

import "@/permissions"

import ElementPlus from 'element-plus'
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'

import directive from "@/directives/HasPermission/HasPermission"

const app = createApp(App)
app
    .use(ElementPlus, {locale: zhCn})
    .use(router)
    .use(store)
    .use(apis)
    .use(components)
    .directive('has-perm', directive)
    .mount('#app')

console.log("import.meta.env====>", import.meta.env)