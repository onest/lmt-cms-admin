import {createRouter, createWebHashHistory} from 'vue-router'
import Layout from '@/layouts/index.vue'


const routes = [
    {
        path: '/',
        name: 'Home',
        component: Layout,
        redirect: '/index',
        meta: {
            title: '首页',
            icon: 'HomeFilled',
            hidden: false,
            oneOnly: true
        },
        children: [
            {
                path: 'index',
                name: 'Index',
                component: () => import('@/views/Home/index.vue'),
                meta: {
                    title: '首页',
                    icon: 'HomeFilled',
                    hidden: false
                },
            }
        ]
    },
    {
        path: '/personal',
        name: 'Personal',
        redirect: '/personal/index',
        component: Layout,
        meta: {
            title: '个人中心',
            icon: 'User',
            hidden: false,
            oneOnly: true
        },
        children: [{
            path: 'index',
            name: 'PersonalIndex',
            component: () => import('@/views/Personal/Personal.vue'),
            meta: {
                title: '个人中心',
                icon: 'User',
                hidden: false
            }
        }]
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('@/views/Login/Login.vue'),
        meta: {
            title: '登录',
            hidden: true
        },
    },
    {
        path: '/404',
        name: '404',
        component: () => import('@/views/404.vue'),
        meta: {
            title: '404',
            hidden: true
        },
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes,
    scrollBehavior(to, from, savedPosition) {
        return savedPosition ? savedPosition : {top: 0}
    }
})

export default router
