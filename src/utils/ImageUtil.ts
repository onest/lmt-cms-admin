export const ImageUtil = {
    compressImage: function (image, quality) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader()
            reader.readAsDataURL(image)
            reader.onload = function (event) {
                const img = new Image()
                img.src = event.target.result
                img.onload = function () {
                    const canvas = document.createElement('canvas')
                    const ctx = canvas.getContext('2d')
                    canvas.width = img.width
                    canvas.height = img.height
                    ctx.drawImage(img, 0, 0, canvas.width, canvas.height)
                    canvas.toBlob((blob) => {
                        const compressedFile = new File([blob], image.name, {
                            type: image.type,
                            lastModified: image.lastModified
                        });
                        resolve(compressedFile);
                    }, image.type || 'image/jpeg', quality)
                }
                img.onerror = reject;
            }
            reader.onerror = reject
        })
    }
}