const routeAllPathToCompMap = import.meta.glob(`../views/**/*.vue`);


// 动态路由
const generateRoutes = (menus: any) => {
    if (menus.length == 0) {
        return;
    }
    let fmtRouters: any[] = [];
    menus.forEach((menu: any) => {

        let {name, routePath, componentPath, children, icon, visiable, type} = menu;

        // 递归格式化children路由
        if (children && children instanceof Array && children.length) {
            children = generateRoutes(children);
        }
        let fmtRouter: any = {
            name: name,
            path: routePath.substring(0, 1) === '/' ? routePath : '/' + routePath,
            meta: {
                title: name,
                icon: icon,
                hidden: visiable == 0
            },
            component: null
        }

        // 目录
        if (type == 'DIRECTORY') {
            if (children && children.length) {
                fmtRouter.redirect = children[0].path
                fmtRouter.component = () => import(`@/layouts/index.vue`)
                fmtRouter.children = children;
                fmtRouter.meta.oneOnly = children.length == 1
            } else {
                fmtRouter.component = routeAllPathToCompMap[`..../views/${componentPath}.vue`];
            }
        } else {
            fmtRouter.component = routeAllPathToCompMap[`../views${componentPath}.vue`];
        }
        fmtRouters.push(fmtRouter);
    });

    return fmtRouters;
}


export default generateRoutes;