import { tr } from "element-plus/es/locale";

/**
 * 验证用户名，5到32位之间，包含字母、数字
 * @param value 验证的值
 * @param rule 规则
 * @returns 
 */
// 自定义验证规则：用户名格式校验
export const validateUsername = (rule, value, callback) => {
    const usernameRegex = /^[a-zA-Z0-9_]{5,32}$/;

    if (!usernameRegex.test(value)) {
        return callback(new Error('用户名必须由5到32位的英文字母、数字或下划线组成，且不能为纯下划线'));
    }

    // 检查是否为纯下划线
    if (/^_+$/.test(value)) {
        return callback(new Error('用户名不能为纯下划线'));
    }

    callback();
};

// export const validateUsername = (value: string, rule) => {
//     const reg = /^(?![0-9]+$)(?![a-zA-Z]+$){5,32}$/;
//     if (!value) {
//         return new Error(rule.message || '请输入用户名');
//     }

//     if (reg.test(value)) {
//         return true
//     } else {
//         return new Error(rule.message || '用户名必须为6-32位数字、字母、特殊符号的组合');
//     }
// }
