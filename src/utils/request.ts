import axios from "axios";
import store from "@/store";
import router from '@/router'
import { ElLoading, ElMessageBox,ElNotification  } from 'element-plus'


const instance = axios.create({
    baseURL: import.meta.env.VITE_APP_BASE_API,
    timeout: 10000,
});

let loading:any = null;

// 请求截器
instance.interceptors.request.use((config) => {
    // 发送请求之前做一些处理
    // 添加token
    if (store.getters.token) {
        config.headers.Authorization = `${store.getters.token}`
    }
    if (config.custom?.showLoading) {
        // 显示loading
        loading = ElLoading.service({
            lock: true,
            text: 'Loading',
            background: 'rgba(0, 0, 0, 0.7)'
        })
    }
    return config;
});

// 响应拦截器
instance.interceptors.response.use(
    // 对响应数据做些事
    (response) => {
        if (loading) {
            loading.close();
        }
        if (response.status === 200) {
            return response.data.data
        } else if (response.status === 401) {
            return Promise.reject(response.data)
        } else {
            return Promise.reject(response.data || { code: 500, message: '未知错误' })
        }
    },
    // 对异常做些事
    (error) => {
        if (loading) {
            loading.close();
        }
        const statusCode = error?.response?.status || 500
        const errorData = error.response?.data || { code: statusCode, message: '未知错误，请稍后重试。' }
        if (statusCode === 404) {
            errorData.message = '请求的资源不存在'
            ElNotification.error({
                title: '温馨提示',
                message: errorData.message,
                type: 'error',
            })
        } else if (statusCode === 401) {
            errorData.message = '您尚未登录，请登录后继续操作'
            ElMessageBox.confirm(errorData.message, '登录', {
                confirmButtonText: '登录',
                cancelButtonText: '取消',
                type: 'error'
            }).then(() => {
                // 跳转登录
                router.push('/login')
            })
        } else if (statusCode === 500) {
            errorData.message = '服务器内部错误'
            ElNotification.error({
                title: '温馨提示',
                message: errorData.message,
                type: 'error',
            })
        } else {
            ElNotification.error({
                title: '温馨提示',
                message: errorData.message|| '未知错误',
                type: 'error',
            })
        }
        if (loading) {
            loading.close();
        }
        return Promise.reject(errorData)
    });

export default instance;