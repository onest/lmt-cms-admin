import { defineComponent } from 'vue'


export default defineComponent({
    data() {
        return {
            tableLoading: false,
            tableData: {
                records: [],
                pageNumber: 1,
                pageSize: 10,
                total: 0,
                totalPage: 0
            },
            search: {
                pageNumber: 1,
                pageSize: 10,
                keywords: null
            },
            dialogFormComponent: {
                name: null,
                data: null,
                mode: null,
                dialogFormVisible: false,
                saveLoading: false,
                title: () => {
                    const name = this.dialogFormComponent.name
                    if (name && name.indexOf('Add') > -1) {
                        return '新增'
                    } else if (name && name.indexOf('Edit') > -1) {
                        return '编辑'
                    } else if (name && name.indexOf('View') > -1) {
                        return '详情'
                    } else {
                        return ""
                    }
                },
                show: (name: string, data: any = null) => {
                    this.dialogFormComponent.name = name
                    this.dialogFormComponent.data = data
                    this.dialogFormComponent.saveLoading = false
                    this.dialogFormComponent.dialogFormVisible = true
                },
                close: () => {
                    this.dialogFormComponent.name = ""
                    this.dialogFormComponent.data = null
                    this.dialogFormComponent.mode = null
                    this.dialogFormComponent.saveLoading = false
                    this.dialogFormComponent.dialogFormVisible = false
                },
                save: () => {
                    console.log("dialog-->save")
                    this.$refs[this.dialogFormComponent.name] &&
                        this.$refs[this.dialogFormComponent.name].save &&
                        this.$refs[this.dialogFormComponent.name].save()
                    this.dialogFormComponent.saveLoading = true
                },
                saveComplete: (state: number) => {
                    console.log("dialog-->saveComplete, state=" + state)
                    this.dialogFormComponent.saveLoading = false
                    if (state == 1) {
                        this.dialogFormComponent.name = ""
                        this.dialogFormComponent.data = null
                        this.dialogFormComponent.dialogFormVisible = false
                        this.getTableData()
                        this.$notify.success({ message: '保存成功', title: '温馨提示' })
                    }
                }
            },
            routePath: this.$route.fullPath
        }
    },
    created() {
    },
    mounted() {
        if (this.$store.state.settings && this.$store.state.settings.params) {
            const search = this.$store.state.settings.params[this.routePath] || undefined
            if (search) {
                this.search = { ...search }
            }
        }

        this.getTableData()
    },
    unmounted() {
        this.$store.dispatch('settings/setParams', { key: this.routePath, value: this.search })
    },
    methods: {
        getTableIndex(index) {
            return (this.search.pageNumber - 1) * this.search.pageSize + index + 1
        },
        getTableData() {
            console.error("请实现 getTableData 方法")
        },
        searchSubmit() {
            this.getTableData()
        },
        showEditForm(data: any = null) {
            this.dialogFormComponent.name = 'EditForm'
            this.dialogFormComponent.mode = 'EDIT'
            this.dialogFormComponent.show('EditForm', data)
        },
        showAddForm(data: any = null) {
            this.dialogFormComponent.name = 'AddForm'
            this.dialogFormComponent.mode = 'ADD'
            this.dialogFormComponent.show('AddForm', data)
        },
        showDeleteForm(data: any) {
            console.error("请实现 showDeleteForm 方法")
        },
        handleSizeChange(size: number) {
            this.search.pageSize = size
            this.search.pageNumber = 1
            this.$store.dispatch('settings/setParams', { key: this.routePath, value: this.search })
            this.getTableData()
        },
        handleCurrentChange(page: number) {
            this.search.pageNumber = page
            this.$store.dispatch('settings/setParams', { key: this.routePath, value: this.search })
            this.getTableData()
        }
    }
})
