import { defineComponent } from 'vue'

export default defineComponent({
    data() {
        return {
            form: {
                id: null
            },
            rules: {},
            saveLoading: false,
            detailLoading: false
        }
    },
    props: {
        data: {
            type: Object,
            default: () => {
            }
        },
        mode: {
            type: String,
            default: 'ADD'
        }
    },
    created() {
        if (this.mode === 'EDIT') {
            this.getDetail()
        }
    },
    methods: {
        getDetail() {
            console.error('请重写 getDetail 方法')
        },
        save() {
            console.log("FormMixin--------> save")
            this.$refs.form.validate((valid) => {
                if (valid) {
                    try {
                        this.saveSubmit()
                    } catch (e) {
                        this.$emit('save-complete', 0)
                    }
                } else {
                    this.$emit('save-complete', 0)
                }
            })
        },
        saveSubmit() {
            console.error('请重写 saveSubmit 方法')
        }
    }
})