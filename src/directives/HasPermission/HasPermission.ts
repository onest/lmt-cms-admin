import { Directive, DirectiveBinding } from "vue";
import store from "@/store"

const checkPermission = (el: HTMLElement, binding: DirectiveBinding) => {
    const { value } = binding
    const permissionList = store.getters.permissions || []
    let permissions = permissionList.map((item: any) => item.permissionCode.toLowerCase())
    let hasPermission = false
    if (permissions && permissions.length) {
        if (value && value instanceof Array) {
            hasPermission = value.some((item: string) => permissions.includes(item))
        } else if (value && typeof value === 'string') {
            hasPermission = permissions.includes(value.toLowerCase())
        }
    }
    if (!hasPermission) {
        el.parentNode && el.parentNode.removeChild(el)
    }
}

const directive: Directive = {
    mounted(el: HTMLElement, binding: DirectiveBinding) {
        checkPermission(el, binding)
    },
    updated(el: HTMLElement, binding: DirectiveBinding) {
        checkPermission(el, binding)
    }
}

export default directive