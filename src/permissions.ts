import router from './router'
import NProgress from 'nprogress'
import store from "@/store/index";
import generateRoutes from '@/utils/RouterUtil';

NProgress.configure({showSpinner: false})

const whiteList = ['/login', '/404']


export const getPageTitle = (title: string) => {
    if (title) return title + '-' + store.getters.siteName
    else return store.getters.title
};

router.beforeEach(async (to, from, next) => {
    NProgress.start()
    let title: string = String(to.meta?.title || '首页');
    document.title = getPageTitle(title)
    let token = store.getters.token
    if (whiteList.includes(to.fullPath)) {
        next()
    } else {
        if (token) {
            let userMenus = store.getters.userMenus
            if (userMenus != null && to.name != null) {
                next()
            } else {
                const menus = store.state.user.userInfo.menus
                userMenus = generateRoutes(menus)
                await store.dispatch('user/setUserMenus', userMenus)
                if (userMenus && userMenus.length) {
                    for (const route of userMenus) {
                        router.addRoute(route)
                    }
                    next({...to, replace: true})
                } else {
                    next("index")
                }
            }
        } else {
            next('/login')
        }
    }
})

router.afterEach(() => {
    NProgress.done()
})