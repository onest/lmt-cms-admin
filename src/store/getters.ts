const getters = {
    siteName: (state: any) => state.settings?.siteName || '测试',
    baseApi: () => import.meta.env.VITE_APP_BASE_API,
    mode: () => import.meta.env.MODE === 'development' ? 'dev' : import.meta.env.MODE == 'staging' ? 'staging' : 'prod',
    token: (state: any) => state.user.token,
    userInfo: (state: any) => state.user.userInfo,
    isCollapse: (state: any) => state.settings.isCollapse,
    openMenu: (state: any) => state.settings.openMenu,
    userMenus: (state: any) => state.user.menus,
    permissions: (state: any) => state.user.userInfo.permissions || [],
}

export default getters