import request from '@/utils/request'

const getDefaultState = () => {
    return {
        token: null,
        userInfo: null,
        menus: null
    }
}

const state = getDefaultState()

const mutations = {
    SET_TOKEN(state: any, token: string) {
        state.token = token
    },
    SET_USER_INFO(state: any, userInfo: object) {
        state.userInfo = userInfo
    },
    SET_USER_MENUS(state: any, menus: object) {
        state.menus = menus
    }
}

const actions = {
    setToken({ commit }: any, token: string) {
        commit('SET_TOKEN', token)
    },
    setUserInfo({ commit }: any, userInfo: object) {
        commit('SET_USER_INFO', userInfo)
    },
    setUserMenus({ commit }: any, menus: object) {
        commit('SET_USER_MENUS', menus)
    },
    login({ commit }: any, form: object) {
        return new Promise((resolve, reject) => {
            request.post('/account/login', form)
                .then((data: any) => {
                    commit('SET_TOKEN', data.token)
                    commit('SET_USER_INFO', data)
                    resolve(data)
                }).catch((error: any) => {
                    reject(error)
                })
        })
    },
    logout({ commit }: any) {
        commit('SET_TOKEN', null)
        commit('SET_USER_INFO', null)
        commit('SET_USER_MENUS', null)
        request.post('/account/logout')
        return Promise.resolve()
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}