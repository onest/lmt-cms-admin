const getDefaultState = () => ({
  siteName: "老木头CMS管理系统",
  isCollapse: false,
  openMenu: null,
  params: null
})

const state = getDefaultState()

const mutations = {
  SET_PARAMS(state, params) {
    if (state.params == null) {
      state.params = {}
    }
    state.params[params.key] = params.value
  },
  CLEAR_PARAMS(state) {
    state.params = null
  }
}

const actions = {
  setParams({ commit }, params) {
    commit("SET_PARAMS", params)
  },
  clearParams({ commit }) {
    commit("CLEAR_PARAMS")
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}