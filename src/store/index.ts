import { createStore } from 'vuex'
import createPersisStore from '@/store/plugins/persisState'
import getters from '@/store/getters'

const modulesFiles = import.meta.glob('./modules/*.ts', {eager: true})

let modules: any = {}

const keys = Object.keys(modulesFiles)
for (const key of keys) {
    let value:any = modulesFiles[key]
    let moduleName = key.replace(/^\.\/(.*)\.\w+$/, '$1')
    const name = moduleName.split('/')[1]
    modules[name] = value.default
}

export default createStore({
    plugins: [createPersisStore({
        key: 'lmt-cms-vuex',
        storage: window.sessionStorage
    })],
    modules,
    getters
})