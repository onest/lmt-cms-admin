
export default function createPersisStore(options = {}) {
    return function (store: any) {
        let { key, storage } = options || { key: 'custom_vuex', storage: window.localStorage }
        let _key = key || 'custom_vuex'
        let _storage = storage || window.localStorage

        // 页面卸载前保存数据
        window.addEventListener('beforeunload', () => {
            _storage.setItem(_key, JSON.stringify(store.state))
        })

        try {
            const localState = _storage.getItem(_key)
            if (localState) {
                store.replaceState(JSON.parse(localState))
            }
        } catch (e) {
            console.error("本地存储数据异常")
        }
    }
}