import request from "@/utils/request";

const routeAllPathToCompMap = import.meta.glob(`./modules/**/*.ts`);

const modules: any[] = []

for (const key of Object.keys(routeAllPathToCompMap)) {
    const module: any = await routeAllPathToCompMap[key]()
    const keys = Object.keys(module)
    for (const key of keys) {
        modules.push({ key, value: module[key] as any })
    }
}


export default {
    install: async (app: any) => {
        app.config.globalProperties.$http = {
            post: request.post,
            get: request.get
        };
        for (let i = 0; i < modules.length; i++) {
            const m: any = modules[i]
            app.config.globalProperties.$http[m.key] = m.value
        }

        console.log("this.$http", app.config.globalProperties.$http)
    }
}