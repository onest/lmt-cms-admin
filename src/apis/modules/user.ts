import request from '@/utils/request';

export const userApi = {
    page: (data: any) => request.post('/user/manage/list', data),
    create: (data: any) => request.post('/user/manage/create', data),
    detail: (id: any) => request.post('/user/manage/detail', { key: id }),
    save: (data: any) => request.post('/user/manage/save', data),
    delete: (id: any) => request.post('/user/manage/delete', { key: id }),
    update: (data: any) => request.post('/user/manage/update', data),
    changeStatus: (id: any) => request.post('/user/manage/changeStatus', { key: id }),
    resetPassword: (id: any) => request.post('/user/manage/resetPassword', { key: id })
}