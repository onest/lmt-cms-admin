import request from "@/utils/request.ts";


export const channelApi = {
    getTreeData: () => request.post("/channel/manage/tree"),
    getDetail: (id: string) => request.post("/channel/manage/detail", { key: id }),
}