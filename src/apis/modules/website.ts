import request from '@/utils/request';


export const websiteApi = {
    list: (data) => request.post("/website/manage/page", data),
    detail: (id) => request.post("/website/manage/detail", {key: id}),
    update: (data) => request.post("/website/manage/update", data),
    delete: (data) => request.post("/website/manage/delete", data),
    create: (data) => request.post("/website/manage/create", data),
}