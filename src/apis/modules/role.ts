import request from '@/utils/request';


export const roleApi: any = {
    // 列表
    page: (data: any) => request.post('/role/manage/page', data),
    // 详情
    detail: (id: any) => request.post('/role/manage/detail', {key: id}),
    // 新增
    add: (data: any) => request.post('/role/manage/create', data),
    // 修改
    update: (data: any) => request.post('/role/manage/update', data),
    // 删除
    delete: (id: any) => request.post('/role/manage/delete', {key: id}),
    // 获取角色菜单权限
    permissions: (id: any) => request.post('/role/manage/permissions', {key:id}),
    // 修改角色菜单权限
    updateRolePermission: (data: any) => request.post('/role/manage/updateRolePermission', data),

}
