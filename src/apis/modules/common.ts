import request from '@/utils/request'


export const commonApi = {
    roleList: () => request.post('/common/roles'),
    getPageTemplates: (key: string) => request.post('/common/template/pageTemplate/getSelectListById',{key})
}