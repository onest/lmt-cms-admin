import request from '@/utils/request';


export const permissionApi: any = {
    // 权限树
    getAll: () => request.post('/permission/manage/tree'),
    // 菜单列表
    menus: () => request.post('/permission/manage/menus'),
    // 详情
    detail: (id: any) => request.post('/permission/manage/detail', {key: id}),
    // 修改
    update: (data: any) => request.post('/permission/manage/update', data),
    // 新增
    create: (data: any) => request.post('/permission/manage/create', data),
    // 删除
    delete: (id: any) => request.post('/permission/manage/delete', {key: id}),
}
