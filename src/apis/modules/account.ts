import request from '@/utils/request';


export const accountApi: any = {
    // 账户登录
    login: (data: any) => request.post('/account/login', data),
    // 账户登出
    logout: () => request.post('/account/logout')
}
