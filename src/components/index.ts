// 如果您正在使用CDN引入，请删除下面一行。
import * as ElementPlusIconsVue from '@element-plus/icons'
import { ElMessage, ElNotification, ElMessageBox } from 'element-plus'
// 注册侧边栏组件
import SideBarItem from '@/layouts/SideBar/SideBarItem.vue'

export default {
    install(app: any) {
        for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
            if (key == 'Menu') {
                app.component("ElMenu", component)
            } else {
                app.component(key, component)
            }
        }
        // 注册侧边栏组件
        app.component("SideBarItem", SideBarItem)
        app.config.globalProperties.$message = ElMessage
        app.config.globalProperties.$notify = ElNotification
        app.config.globalProperties.$alert = ElMessageBox.alert
        app.config.globalProperties.$confirm = ElMessageBox.confirm
        app.config.globalProperties.$prompt = ElMessageBox.prompt
        app.config.globalProperties.$messageBox = ElMessageBox
    }
}

