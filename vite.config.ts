import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
// @ts-ignore
import path from 'path';


import Unocss from 'unocss/vite'


const pathResolve = (pathStr: String) => {
  // @ts-ignore
  return path.resolve(__dirname, pathStr);
}

console.log(import.meta)

// const baseApi = import.meta.env.VITE_APP_BASE_API
// const baseApiHost = import.meta.env.VITE_APP_BASE_API_HOST


// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    AutoImport({ resolvers: [ElementPlusResolver()] }),
    Components({ resolvers: [ElementPlusResolver()] }),
    Unocss()
  ],
  resolve: {
    alias: {
      '@': pathResolve('./src'),
    }
  },
  build: {
    sourcemap: false,
    minify: 'esbuild', // 构建时的压缩方式
    rollupOptions: {
      output: {
        chunkFileNames: 'js/[name]-[hash].js',
        entryFileNames: 'js/[name]-[hash].js',
        assetFileNames: '[ext]/[name]-[hash].[ext]',
      }
    }
  },
  server: {
    port: 9000, // 设置服务启动端口号
    open: true, // 设置服务启动时是否自动打开浏览器
    proxy: {
      '/dev-api': {
        changeOrigin: true, // 是否跨域
        rewrite: (path) => path.replace(/^\/dev-api/, ''),
        target: {
          host: 'localhost',// 后端开发人员ip地址
          port: 8080,       // 后端开发人员监听端口号
          protocol: 'http', // 协议
        }
      }
    }
  }
})



